(function ($) {
  Drupal.behaviors.expander = {
    attach: function (context, settings) {
      var mobile_width = Drupal.settings.expander.mobile_width || 0;

      if (mobile_width == 0 || $(window).width() <= mobile_width) {
        $('.expander-wrapper').removeClass('expanded');

        $('.expander-wrapper a.trigger-btn').click(function(e) {
          e.preventDefault();

          $(this).closest('.expander-wrapper').toggleClass('expanded');
        });
      }
    }
  };
})(jQuery);
