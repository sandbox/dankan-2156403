<?php

/**
 * @file
 * Wysiwyg API integration on behalf of Expander module.
 */

/**
 * Implements of hook_INCLUDE_plugin();
 */
function expander_expand_plugin() {
  $plugins['expand'] = array(
    'title' => t('Expander plugin'),
    'icon file' => 'expand.gif',
    'icon title' => t('Add expand link that divides the text on the output.'),
    'js file' => 'expand.js',
    'css file' => 'expand.css',
    'settings' => array(
      'breakhtml' => EXPANDER_BREAKER_HTML,
    ),
  );
  return $plugins;
}
