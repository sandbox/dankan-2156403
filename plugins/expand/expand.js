(function ($) {

// @todo Array syntax required; 'break' is a predefined token in JavaScript.
Drupal.wysiwyg.plugins['expand'] = {

  /**
   * Return whether the passed node belongs to this plugin.
   */
  isNode: function(node) {
    return ($(node).is('img.wysiwyg-expander'));
  },

  /**
   * Execute the button.
   */
  invoke: function(data, settings, instanceId) {
    var re = new RegExp(settings.breakhtml, 'g');

    if (data.format == 'html') {
      // Prevent duplicating a teaser break.
      if ($(data.node).is('img.wysiwyg-expander')) {
        return;
      }
      var content = this._getPlaceholder(settings);
    }
    else {
      // Prevent duplicating a teaser break.
      // @todo data.content is the selection only; needs access to complete content.
      if (data.content.match(re)) {
        return;
      }
      var content = settings.breakhtml;
    }
    if (typeof content != 'undefined') {
      Drupal.wysiwyg.instances[instanceId].insert(content);
    }
  },

  /**
   * Replace all <span class="expander"></span> tags with images.
   */
  attach: function(content, settings, instanceId) {
    var re = new RegExp(settings.breakhtml, 'g');

    content = content.replace(re, this._getPlaceholder(settings));
    return content;
  },

  /**
   * Replace images with <span class="expander"></span> tags in content upon detaching editor.
   */
  detach: function(content, settings, instanceId) {
    var $content = $('<div>' + content + '</div>');
    $.each($('img.wysiwyg-expander', $content), function (i, elem) {
      $(this).before(settings.breakhtml).remove();
    });
    return $content.html();
  },

  /**
   * Helper function to return a HTML placeholder.
   */
  _getPlaceholder: function (settings) {
    return '<img src="' + settings.path + '/images/spacer.gif" alt="&lt;--break-&gt;" title="&lt;--break--&gt;" class="wysiwyg-expander drupal-content" />';
  }
};

})(jQuery);
